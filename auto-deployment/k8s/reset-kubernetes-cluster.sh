sudo kubeadm reset
read -p 'Kubernetes IP Address, followed by [ENTER]: ' IP
sudo kubeadm init --pod-network-cidr=10.244.0.0/16 --apiserver-advertise-address=$IP
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
#kubectl apply -f calico/calico.yaml
#kubectl apply -f flannel/flannel.yml
kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
echo "#######  ONLY FOR SINGLE NODE CLUSTER  #######"
kubectl taint nodes --all node-role.kubernetes.io/master-
echo "#######  Install HELM CONFIG  #######"
kubectl create serviceaccount --namespace kube-system tiller
kubectl create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller
helm init --service-account tiller --override spec.selector.matchLabels.'name'='tiller',spec.selector.matchLabels.'app'='helm' --output yaml | sed 's@apiVersion: extensions/v1beta1@apiVersion: apps/v1@' | kubectl apply -f -
until helm ls >& /dev/null; \
do \
  echo "Waiting for Helm to be ready"; \
  sleep 5; \
done