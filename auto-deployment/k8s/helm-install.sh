curl -L -o /tmp/helm.tgz "https://storage.googleapis.com/kubernetes-helm/helm-v2.14.2-linux-amd64.tar.gz"
echo "9f50e69cf5cfa7268b28686728ad0227507a169e52bf59c99ada872ddd9679f0  /tmp/helm.tgz" | sha256sum -c -
cd /tmp; tar -xzvf helm.tgz; sudo mv linux-amd64/helm /usr/local/bin/helm
sudo chmod a+x /usr/local/bin/helm
rm -rf /tmp/helm.tgz /tmp/linux-amd64