setenforce 0
swapoff -a
sed -i --follow-symlinks 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/sysconfig/selinux
cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF
yum update
yum -y install kubelet-1.12.7-* kubeadm-1.12.7-* kubectl-1.12.7-*
systemctl start kubelet && systemctl enable kubelet
######### RE-INSTALL K8S COMPLETED ###########