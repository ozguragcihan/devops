yum update
subscription-manager repos --enable=rhel-7-server-extras-rpms
yum update
yum install -y yum-utils device-mapper-persistent-data lvm2
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
yum install -y docker-ce-17.06*
usermod -aG docker $USER
systemctl start docker && systemctl enable docker