    
1)  Clone SEPON devops project
    ```
    git clone --single-branch --branch master https://gitlab.com/ozguragcihan/devops.git
    
2)  Install Docker 
    ```
    cd devops/auto-deployment/centos or cd devops/auto-deployment/ubuntu
    sudo su -
    ./install-docker.sh
    exit
    ```

3)  Install Kubernetes
    ```
    cd devops/auto-deployment/centos or cd devops/auto-deployment/ubuntu
    sudo su -
    ./install-k8s.sh
    exit
    ```
3)  Initialize Kubernetes Cluster
    ```
    cd devops/auto-deployment/k8s
    sudo su -
    ./reset-kubernetes-cluster.sh
    Note : It will ask, new kubernetes IP Address, please enter it correctly.
    This script also will create tiller and helm pods. But Helm should be installed under /user/local/bin/helm. After once runnig make argela, helm should be copied correct path.
    ```
    ** But First if you do not have helm **
    ```
    cd devops/auto-deployment/ks8
    sudo su -
    ./helm-install.sh
    exit
    ```

4)  Depoloy Pods Example : MYSQL
    ```
    cd devops/YML/mysql
    kubectl apply -f mysql-pvc.yml
    kubectl apply -f mysql-deployment.yml